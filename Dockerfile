ARG ALPINE_VERSION=3.21
ARG PB_VERSION
FROM alpine:${ALPINE_VERSION} as package

ENV PB_VERSION 0.24.2

RUN wget \ 
    https://github.com/pocketbase/pocketbase/releases/download/v${PB_VERSION}/pocketbase_${PB_VERSION}_linux_amd64.zip \
    -O /tmp/pb.zip \
    && unzip /tmp/pb.zip

FROM alpine:${ALPINE_VERSION}

RUN apk update \
    && apk add ca-certificates git tzdata tini \
    && rm -rf /var/cache/apk/*

COPY --from=package /pocketbase /usr/local/bin/pocketbase

ENV TZ=Asia/Jakarta
EXPOSE 8080

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/usr/local/bin/pocketbase", "serve", "--http=0.0.0.0:8080", "--dir=/pb_data", "--publicDir=/pb_public"]
